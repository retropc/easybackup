import json
from dataclasses import dataclass
from typing import Optional, List, Any, Dict, Callable, TypeVar

VALID_NAME_CHARS = frozenset("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-.")

JsonType = Any
T = TypeVar("T")

@dataclass
class Backup:
  name: str
  extension: str
  key: str
  keep: int
  hourly: bool
  root: str
  rate: Optional[int]
  hard_size_limit: Optional[int]
  soft_size_limit: Optional[int]

  @classmethod
  def from_json(cls, name: str, j: JsonType, defaults: JsonType) -> 'Backup':
    if set(name) - VALID_NAME_CHARS:
      raise Exception(f"invalid name: {name}")
    def optional(k: str, t: Callable[[Any], T], default: Optional[T]=None) -> Optional[T]:
      if k in j:
        return t(j[k])
      if k in defaults:
        return t(defaults[k])
      return default
    def required(k: str, t: Callable[[Any], T], default: Optional[T]=None) -> T:
      v = optional(k, t, default)
      if v is None:
        raise Exception(f"backup {name} (or defaults section) has missing required key: {k}")
      return v
    return cls(name=name, extension=required("extension", str), keep=required("keep", int), key=required("key", str), hourly=required("hourly", bool, False), root=required("root", str), rate=optional("rate", lambda x: int(x) * 1024), hard_size_limit=optional("hard_size_limit", int), soft_size_limit=optional("soft_size_limit", int))

@dataclass
class Config:
  script_path: str
  python_path: str
  backups: Dict[str, Backup]

  @classmethod
  def from_json(cls, j: JsonType) -> 'Config':
    script_path = j["script_path"]
    defaults = j.get("defaults", {})

    backups = {}
    for k, v in j["backups"].items():
      backups[k] = Backup.from_json(k, v, defaults)

    return cls(script_path=script_path, backups=backups, python_path=j["python_path"])

  @classmethod
  def from_file(cls, filename: str) -> 'Config':
    with open(filename, "r") as f:
      return cls.from_json(json.load(f))
