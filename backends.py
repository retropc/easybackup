import os
from abc import ABC, abstractmethod
from typing import List, BinaryIO, Any, Optional

from util import warn

class StorageBackend(ABC):
  @abstractmethod
  def write(self, filename: str, stream: BinaryIO) -> str:
    pass

  @abstractmethod
  def list(self) -> List[str]:
    pass

  @abstractmethod
  def remove(self, filename: str) -> None:
    pass

class DiskBackend(StorageBackend):
  def __init__(self, target_dir: str) -> None:
    self.target_dir = target_dir

  def write(self, filename: str, stream: BinaryIO) -> str:
    if not os.path.exists(self.target_dir):
      os.makedirs(self.target_dir)

    with open(os.path.join(self.target_dir, filename), "wb") as f:
      while True:
        b = stream.read(16384)
        if not b:
          break
        f.write(b)
      os.fsync(f.fileno())

    return filename

  def list(self) -> List[str]:
    return os.listdir(self.target_dir)

  def remove(self, filename: str) -> None:
    os.unlink(os.path.join(self.target_dir, filename))

class S3Backend(StorageBackend):
  def __init__(self, bucket_name: str, prefix: str, variant: str = "s3", max_bandwidth: Optional[int]=None) -> None:
    import boto3 # type: ignore

    client_kwargs = {}
    if variant == "gs":
      client_kwargs["endpoint_url"] = "https://storage.googleapis.com"
    elif variant == "s3":
      pass
    else:
      raise Exception(f"unknown variant: {variant}")

    self.s3 = boto3.client(service_name="s3", **client_kwargs)

    kwargs = {}
    if max_bandwidth is not None:
      kwargs["max_bandwidth"] = max_bandwidth
    self.transfer_config = boto3.s3.transfer.TransferConfig(use_threads=False, max_io_queue=10, **kwargs)

    self.bucket_name = bucket_name
    self.prefix = prefix

  def write(self, filename: str, stream: BinaryIO) -> str:
    target_file = self.prefix + "/" + filename
    self.s3.upload_fileobj(stream, self.bucket_name, target_file, Config=self.transfer_config)
    return target_file

  def list(self) -> List[str]:
    response = self.s3.list_objects_v2(Bucket=self.bucket_name, Prefix=self.prefix + "/")
    if response["IsTruncated"]:
      warn("more keys in S3 than could be returned")
    if "Contents" not in response:
      return []

    # s3 sorts for us: "Objects are returned sorted in an ascending order of the respective key names in the list."
    l = [contents["Key"] for contents in response["Contents"]]

    # check it actually does...
    l2 = sorted(l)
    if l != l2:
      warn("S3 assertion failure -- continuing anyway...")

    return l2

  def remove(self, filename: str) -> None:
    self.s3.delete_object(Bucket=self.bucket_name, Key=filename)
