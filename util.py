import syslog
import time
import sys
from typing import BinaryIO, Optional, Sequence
from abc import ABC, abstractmethod

def info(x: str) -> None:
  syslog.syslog(x)

def warn(x: str) -> None:
  syslog.syslog("warning: %s" % x)
  print("warning: %s" % x, file=sys.stderr)

class ChunkHandler(ABC):
  @abstractmethod
  def data(self, l: int) -> None:
    pass

class RateLimiter(ChunkHandler):
  WINDOWS = 10
  def __init__(self, max_rate_bps: int) -> None:
    self.rate = max_rate_bps
    self.check_every = max_rate_bps // self.WINDOWS
    self.last_time = time.time()
    self.bytes = 0

  def data(self, l: int) -> None:
    self.bytes+=l
    if self.bytes < self.check_every:
      return

    now = time.time()
    delta_time = now - self.last_time
    expected = self.rate * delta_time

    if self.bytes > expected:
      sleep_for = (self.bytes - expected) / self.rate
      time.sleep(sleep_for)
      now = time.time()

    self.last_time = now
    self.bytes = 0

class SizeLimit(ABC):
  @abstractmethod
  def hit_limit(self, limit: int) -> None:
    pass

class SizeLimiter(ChunkHandler, SizeLimit):
  def __init__(self, limit: int, hit: Optional[SizeLimit]=None) -> None:
    self.hit: SizeLimit
    if hit is None:
      self.hit = self
    else:
      self.hit = hit
    self.limit = limit
    self.size = 0

  def hit_limit(self, limit: int) -> None:
    raise Exception(f"Maximum size limit exceeded: {limit}")

  def __check(self) -> None:
    if self.size >= self.limit:
      self.hit.hit_limit(self.limit)

  def data(self, l: int) -> None:
    self.__check()

    self.size += l

    self.__check()

class ChunkHandlersReader:
  def __init__(self, chunk_handlers: Sequence[ChunkHandler], f: BinaryIO):
    self.f = f
    self.chunk_handlers = list(chunk_handlers)

  def __chunk(self, l: int) -> None:
    for c in self.chunk_handlers:
      c.data(l)

  def read(self, l: Optional[int]=None) -> bytes:
    if l is None:
      return self._read_until_eof()

    buf = b''
    while l > 0:
      to_read = min(8192, l)
      data = self.f.read(to_read)
      if not data:
        break
      buf += data
      l -= len(data)
      self.__chunk(len(data))

    return buf

  # s3transfer does a weird read() (with no arg) if the first chunk < multipart upload size
  def _read_until_eof(self) -> bytes:
    buf = b''
    while True:
      data = self.f.read(8192)
      if not data:
        break
      buf += data
      self.__chunk(len(data))

    return buf
